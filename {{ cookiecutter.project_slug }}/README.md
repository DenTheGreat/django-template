# {{ cookiecutter.project_name }}

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

### First run: ###

Install Python {{ cookiecutter.python_version }} & setup virtual environment. We recommend to use [pyenv](https://github.com/pyenv/pyenv) & [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv):

```bash
pyenv install {{ cookiecutter.python_version }}
pyenv virtualenv {{ cookiecutter.python_version }} {{ cookiecutter.project_slug }}
pyenv activate {{ cookiecutter.project_slug }}
```

Update `pip` & `setuptools`, install `fabric`, `invoke` & `pip-tools`:

```bash
pip install -U pip pip-tools setuptools
```

Install Python requirements: // TODO: Rework

```bash
fab pip.sync
```

Copy initial settings for Django project:

```bash
cp .env.example .env
```

Generate `SECRET_KEY`:

```bash
python manage.py generate_secret_key
```

and write it to `./api/.env`:

```
{{ cookiecutter.project_slug | upper() }}_SECRET_KEY=<your-generated-key>
```

Run backing services (require Docker):

```bash
fab compose.up -d
```